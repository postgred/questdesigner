@app.controller('QuestoinsCtrl', ($scope, QuestionsStore) ->

  QuestionsStore.loadQuestions()

  this.addQuestion = () ->
    QuestionsStore.addQuestion(this.newQuestion)
    this.resetQuestion()

  this.resetQuestion = () ->
    this.newQuestion = {
      text: ''
    }

  this.questions = QuestionsStore.questions
  this.resetQuestion()

  return
)
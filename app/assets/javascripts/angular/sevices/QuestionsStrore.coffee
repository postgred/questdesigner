@app.factory 'QuestionsStore', (Restangular) ->
  questions: []
  loadQuestions: () ->
    this.questions = Restangular.all('questions').getList().$object
  addQuestion: (question) ->
    that = this
    return Restangular.all('questions').post({question: question}).then( () ->
      that.questions.push(question)
    )
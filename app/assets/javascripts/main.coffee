@app = angular.module('app', ['ui.router', 'restangular', 'ng-rails-csrf'])
  .config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
    $stateProvider
      .state('root', {
        url: '/',
        templateUrl: 'templates/index.html',
        controller: 'QuestoinsCtrl'
      })
    $urlRouterProvider.otherwise('root')
  ])